import User from '../dataObject/User';
import cat from '../../ressources/cat.json';
import Menu from './Menu';
import Router from '../Router';
import Categorie from '../dataObject/Categorie';
import vente from '../../ressources/vente.json';

export default class StatCategoriesPage extends Menu {
	categories = [];

	constructor() {
		super('StatCategoriesPage');
		cat.forEach(cat => {
			this.categories.push(new Categorie(cat.categorie, cat.sousCat));
		});
	}

	mount(element) {
		super.mount(element);
		element.innerHTML = this.render();
		document
			.querySelector('.deconnectionButton')
			.addEventListener('click', this.submit);
		document.querySelector('.rdv').addEventListener('click', this.goToRdv);
		document.querySelector('.user').addEventListener('click', this.goToUser);
	}

	goToRdv(event) {
		event.preventDefault();
		Router.navigate('/rdvTable');
	}

	goToUser(event) {
		event.preventDefault();
		Router.navigate('/users');
	}

	render() {
		return `
        <header>
		    <nav>
			    <button type="submit" class="deconnectionButton">Se deconnecter</button>
                <button type="submit" class="rdv">Liste rdv</button>
				<button type="submit" class="user">Liste user</button>
			
		    </nav>
	    </header>
	    <section class="pageContainer">
		    <header class="pageTitle"></header>
            ${this.getTab()}
		    <div class="pageContent"></div>
	    </section>`;
	}

	getTab() {
		let res = '<table>';
		this.categories.forEach(cate => {
			res += `<tr>${this.getStatsRow(cate)}</tr>`;
		});
		res += `<tr>
		<th>Nombre de vente total: </th>
		<th>${this.getVenteTotal()}</th>
		</tr>`;

		return (res += '</table>');
	}

	getStatsRow(cate) {
		let html = `
	    <th><a href="stat/${cate.name}">${cate.name}</a></th>;
		<th>nombre de vente: ${this.getVenteByCategorie(cate)}</th>`;
		return html;
	}

	getVenteBySousCategorie(cate) {
		let nbr = 0;
		vente.forEach(previousSale => {
			if (cate.categorie == previousSale.categorie) {
				if (cate.sousCat == previousSale.sousCat) {
					nbr++;
				}
			}
		});
		return nbr;
	}

	getVenteByCategorie(cate) {
		let nbr = 0;
		vente.forEach(previousSale => {
			if (cate.name == previousSale.categorie) {
				nbr++;
			}
		});
		return nbr;
	}

	getVenteTotal() {
		return vente.length;
	}
}
