import Router from '../Router';
import Page from './Page';
export default class Menu extends Page {
	constructor(name) {
		if (name == null) {
			super('MenuPage');
		} else {
			super(name);
		}
	}

	mount(element) {
		super.mount(element);
	}

	render() {
		return `<header>
		<button type="submit" class="deconnectionButton">
						<span class="button__text">Se deconnecter</span>
						<i class="button__icon fas fa-chevron-right"></i>
		</button>
		<nav>
			<a class="autreAdmin" href="http://veictor6.azae.eu">Page admin échange</a>
			<a href="/rdvTable">Liste des rendez-vous</a>
			<a href="/users"> Liste des utilisateurs</a>
			<a href="/stats">Statistiques</a>
			
		</nav>
		</header>
	`;
	}

	submit(event) {
		event.preventDefault();
		super.disconnect();
		Router.navigate('/connexion');
	}

	getFormFieldValue(fieldName) {
		const input = this.element.querySelector(`input[name=${fieldName}]`),
			res = input.value;
		return res;
	}
}
