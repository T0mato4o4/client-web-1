import User from '../dataObject/User';
import Router from '../Router';
import Page from './Page';
export default class Connection extends Page {
	constructor() {
		super('connectionPage');
	}

	mount(element) {
		super.mount(element);

		element.innerHTML = this.render();
		const form = this.element.querySelector('form');
		form.addEventListener('submit', event => this.submit(event));
	}

	render() {
		return `<div class="container">
		<div class="screen">
			<div class="screen__content">
				<form class="login">
					<div class="login__field">
						<i class="login__icon fas fa-user"></i>
						<input type="text" name="pseudo" class="login__input" placeholder="nom d'utilisateur">
					</div>
					<div class="login__field">
						<i class="login__icon fas fa-lock"></i>
						<input type="password" name="mdp" class="login__input" placeholder="mot de passe">
					</div>
					<button type="submit" class="button login__submit">
						<span class="button__text">Se connecter</span>
						<i class="button__icon fas fa-chevron-right"></i>
					</button>		
				</form>
				
			</div>
			<div class="screen__background">
				<span class="screen__background__shape screen__background__shape4"></span>
				<span class="screen__background__shape screen__background__shape3"></span>		
				<span class="screen__background__shape screen__background__shape2"></span>
				<span class="screen__background__shape screen__background__shape1"></span>
			</div>		
		</div>
	</div>`;
	}

	submit(event) {
		event.preventDefault();
		const pseudo = this.getFormFieldValue('pseudo');
		const mdp = this.getFormFieldValue('mdp');
		document.cookie = `VeictorPseudo=${pseudo}; SameSite=Strict`;
		document.cookie = `VeictorMdp=${mdp};SameSite=Strict`;
		Router.navigate('/rdvTable');
	}

	getFormFieldValue(fieldName) {
		const input = this.element.querySelector(`input[name=${fieldName}]`),
			res = input.value;
		return res;
	}
}
