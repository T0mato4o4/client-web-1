import Menu from './Menu';
import RendezVous from '../dataObject/RendezVous';
import Router from '../Router';
import PageNotFoud from './PageNotFound';
import User from '../dataObject/User';

export default class RdvTablePage extends Menu {
	rdvData;

	constructor() {
		super('RdvTablePage');
	}

	mount(element, pathPart) {
		this.rdvData = [];
		if (pathPart == null || pathPart[2].length == 0) {
			/*fetch('http://veictor11.azae.eu/api/v1/rendezvous')
				.then(response => response.json())
				.then(responseData => {
					this.treatData(element, responseData);
				});*/
			fetch('http://veictor11.azae.eu/api/v1/rendezvous/detail')
				.then(response => response.json())
				.then(responseData => {
					this.rdvData = RendezVous.createRdvList(responseData);
					super.mount(element);
					element.innerHTML = this.render();
					document
						.querySelector('.deconnectionButton')
						.addEventListener('click', this.submit);
					this.setLink();
					document
						.querySelector(
							`a[href="${'/' + window.location.pathname.split('/')[1]}"]`
						)
						.classList.add('active');
				});
		} else if (pathPart.length == 4) {
			fetch(
				`http://veictor11.azae.eu/api/v1/rendezvous/${pathPart[2]}/${pathPart[3]}`
			)
				.then(response => {
					if (!response.ok) {
						throw new Error(`Erreur : ${response.statusText}`);
					}
					return response.json();
				})
				.then(responseData => {
					//this.treatData(element, responseData);
					const idList = [];
					const rdvJSONList = [];
					responseData.forEach(rdv => {
						idList.push(rdv.id);
					});
					fetch('http://veictor11.azae.eu/api/v1/rendezvous/detail')
						.then(responseDetail => responseDetail.json())
						.then(responseDetailData => {
							responseDetailData.forEach(rdv => {
								console.log(rdv);
								if (idList.includes(rdv.id)) {
									console.log('cirno');
									rdvJSONList.push(rdv);
								}
								console.log('9');
							});
							this.rdvData = RendezVous.createRdvList(rdvJSONList);
							super.mount(element);
							element.innerHTML = this.render();
							document
								.querySelector('.deconnectionButton')
								.addEventListener('click', this.submit);
							this.setLink();
							document
								.querySelector(
									`a[href="${'/' + window.location.pathname.split('/')[1]}"]`
								)
								.classList.add('active');
						});
				})
				.catch(error => {
					new PageNotFoud().mount(element);
				});
		} else {
			new PageNotFoud().mount(element);
		}
	}
	render() {
		return `${super.render()}
	<section class="pageContainer">
		<header class="pageTitle"></header>
        ${this.getTab()}
		<div class="pageContent"></div>
	</section>`;
	}

	getTab() {
		let res = `<table class="table table-striped">
		<thead class="thead-dark">
		<tr>
		<th>ID rendez-vous</th>
		<th>ID vendeur</th>
		<th>Vendeur</th>
		<th>Age vendeur</th>
		<th>Genre vendeur</th>
		<th>ID acheteur</th>
		<th>Acheteur</th>
		<th>Age acheteur</th>
		<th>Genre acheteur</th>
		<th>Date</th>
		<th>Lieu</th>
		</tr>
		</thead>
		`;
		this.rdvData =
			this.rdvData == null ? RendezVous.getRdvList() : this.rdvData;
		this.rdvData.forEach(rdv => {
			res += `<tr>${this.getRdvRow(rdv)}</tr>`;
		});

		return (res += '</table>');
	}

	getRdvRow(rdv) {
		const vendeur = rdv.vendeur;
		const acheteur = rdv.acheteur;
		return `<th>${rdv.id}</th>
        <th><a href="/rdvTable/user/${vendeur.veictorienId}">${
			vendeur.veictorienId
		}</a></th>
        <th><a href="/rdvTable/user/${vendeur.veictorienId}">${
			vendeur.prenom
		} ${vendeur.nom}</a>
		</th>
        <th>${vendeur.age} ans</th><th>${User.getGenre(vendeur.genre)}</th>
        <th><a href="/rdvTable/user/${acheteur.veictorienId}">${
			acheteur.veictorienId
		}</a></th>
        <th><a href="/rdvTable/user/${acheteur.veictorienId}">${
			acheteur.prenom
		} ${acheteur.nom}</a></th><th>${acheteur.age} ans</th>
        <th>${User.getGenre(acheteur.genre)}</th>
        <th>${rdv.getCompleteDate()}</th>
        <th>${rdv.lieu}</th>
        `;
	}

	setLink() {
		const links = this.element.querySelectorAll('a:not(.autreAdmin)');
		links.forEach(link => {
			link.addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(event.currentTarget.getAttribute('href'));
			});
		});
	}

	treatData(element, responseData, lastIdx) {
		const idx = lastIdx == null || lastIdx < 0 ? 0 : lastIdx + 1;
		if (idx < responseData.length) {
			fetch(
				`http://veictor11.azae.eu/api/v1/rendezvous/detail/${responseData[idx].id}`
			)
				.then(responseDetails => responseDetails.json())
				.then(responseDetailsData => {
					const newRedv = RendezVous.createFromRdvAndDetails(
						responseData[idx],
						responseDetailsData
					);
					this.rdvData.push(newRedv);

					this.treatData(element, responseData, idx);
				});
		} else {
			super.mount(element);
			element.innerHTML = this.render();
			document
				.querySelector('.deconnectionButton')
				.addEventListener('click', this.submit);
			this.setLink();
		}
	}
}
