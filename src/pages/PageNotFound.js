import Page from './Page';
import Router from '../Router';
export default class PageNotFoud extends Page {
	constructor() {
		super('PageNotFound');
	}
	mount(element) {
		super.mount(element);
		element.innerHTML = `<h1><b>ERROR 404: PAGE NOT FOUND</b></br> <a href="/rdvTable"> RETOUR </a><h1>`;
		element.querySelector('a').addEventListener('click', event => {
			event.preventDefault();
			Router.navigate(event.currentTarget.getAttribute('href'));
		});
	}
}
