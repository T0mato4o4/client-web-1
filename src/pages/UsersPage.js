import User from '../dataObject/User';
import user from '../../ressources/users.json';
import Menu from './Menu';
import Router from '../Router';

export default class UsersPage extends Menu {
	users = [];

	constructor() {
		super('UsersPage');
		user.forEach(user => {
			this.users.push(new User(user.id, user.nom, user.prenom, user.genre));
		});
	}

	mount(element) {
		super.mount(element);
		fetch('http://veictor11.azae.eu/api/v1/utilisateurs')
			.then(response => response.json())
			.then(responseData => {
				element.innerHTML = this.render(responseData);
				document
					.querySelector('.deconnectionButton')
					.addEventListener('click', this.submit);

				this.setLink();
				document
					.querySelector(
						`a[href="${'/' + window.location.pathname.split('/')[1]}"]`
					)
					.classList.add('active');
			});
	}

	render(userJSONList) {
		return `
        ${super.render()}
	    <section class="pageContainer">
		    <header class="pageTitle"></header>
            ${this.getTab(userJSONList)}
		    <div class="pageContent"></div>
	    </section>`;
	}

	getTab(userJSONList) {
		let res = `<table  class="table table-striped"> <thead class="thead-dark"><tr>
			<th>ID</th>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Age</th>
			<th>Genre</th>
		</tr></thead>`;
		userJSONList.forEach(userJSON => {
			const user = new User(
				userJSON.id_veictorien,
				userJSON.nom,
				userJSON.prenom,
				userJSON.age,
				userJSON.genre
			);
			res += `<tr>${this.getUsersRow(user)}</tr>`;
		});

		return (res += '</table>');
	}

	getUsersRow(user) {
		return `
	    <th><a href="/rdvTable/user/${user.id}">${user.id}</a></th>
	    <th><a href="/rdvTable/user/${user.id}">${user.nom}</a></th>
		<th><a href="/rdvTable/user/${user.id}">${user.prenom}</a></th>
	    <th>${user.age}</th>
	    <th>${user.getGenre()}</th>
	    `;
	}

	setLink() {
		const links = this.element.querySelectorAll('a:not(.autreAdmin)');
		links.forEach(link => {
			link.addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(event.currentTarget.getAttribute('href'));
			});
		});
	}
}
