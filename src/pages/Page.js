export default class Page {
	className;
	element;
	admins = [];

	constructor(className) {
		this.className = className;
	}
	mount(element) {
		this.element = element;
	}

	render() {
		return `<section class="${this.className}">${this.element}</section>`;
	}

	disconnect() {
		document.cookie =
			'VeictorPseudo=; expires=Thu, 01 Jan 1970 00:00:00 UTC;SameSite=Strict;';
		document.cookie =
			'VeictorMdp=; expires=Thu, 01 Jan 1970 00:00:00 UTC;SameSite=strict;';
	}
}
