import Menu from './Menu';
import User from '../dataObject/User';
import Router from '../Router';
import RendezVous from '../dataObject/RendezVous';

export default class StatsPage extends Menu {
	users;
	rdvs;
	idxYear;
	dateData;
	constructor() {
		super('statPage');
	}
	mount(element) {
		super.mount(element);
		this.users = [];
		this.rdvs = [];
		this.idxYear = 0;

		fetch('http://veictor11.azae.eu/api/v1/utilisateurs')
			.then(response => response.json())
			.then(responseData => {
				responseData.forEach(userJSON => {
					this.users.push(
						new User(
							userJSON.id,
							userJSON.nom,
							userJSON.prenom,
							userJSON.age,
							userJSON.genre
						)
					);
				});
				fetch('http://veictor11.azae.eu/api/v1/rendezvous')
					.then(rdvResponse => rdvResponse.json())
					.then(rdvResponseData => {
						this.rdvs = RendezVous.createRdvList(rdvResponseData);
						this.dateData = this.getDateData();
						element.innerHTML = this.render();
						document
							.querySelector('.deconnectionButton')
							.addEventListener('click', this.submit);
						this.setLink();
						console.log(window.location.pathname);
						document
							.querySelector(
								`a[href="${'/' + window.location.pathname.split('/')[1]}"]`
							)
							.classList.add('active');
						const nextButton = element.querySelector('#nextButton');
						const actualYearButton = element.querySelector('#actualYearButton');
						nextButton.disabled = true;
						actualYearButton.disabled = true;
						element
							.querySelector('#prevButton')
							.addEventListener('click', event => {
								event.preventDefault();
								this.idxYear--;
								nextButton.disabled = this.idxYear == 0;
								actualYearButton.disabled = this.idxYear == 0;
								element.querySelector('#yearTable').innerHTML =
									this.getStatsDateYearBetween(
										new Date().getFullYear() - 10 + this.idxYear,
										new Date().getFullYear() + this.idxYear
									);
							});
						actualYearButton.addEventListener('click', event => {
							event.preventDefault();
							if (this.idxYear != 0) {
								this.idxYear = 0;
							}
							element.querySelector('#yearTable').innerHTML =
								this.getStatsDateYearBetween(
									new Date().getFullYear() - 10 + this.idxYear,
									new Date().getFullYear() + this.idxYear
								);
							actualYearButton.disabled = true;
							nextButton.disabled = this.idxYear == 0;
						});

						nextButton.addEventListener('click', event => {
							event.preventDefault();
							if (this.idxYear < 0) {
								this.idxYear++;
								actualYearButton.disabled = this.idxYear == 0;
								element.querySelector('#yearTable').innerHTML =
									this.getStatsDateYearBetween(
										new Date().getFullYear() - 10 + this.idxYear,
										new Date().getFullYear() + this.idxYear
									);
								if (this.idxYear == 0) {
									nextButton.disabled = true;
								}
							}
						});
					});
			});
	}
	render() {
		let res = `${super.render()}
        <section class="statsSection"><h1>TOTAL D'UTILISATEURS:${
					this.users.length
				}</h1></br><section id="section1">${this.getStatGenre()} ${this.getStatAge()}</section>${this.getStatDateMois()}${this.getStatsYear()}
	    </section>`;

		return res;
	}
	getStatGenre() {
		const nbUser = this.users.length;
		const genreTab = [0, 0, 0];
		this.users.forEach(user => {
			genreTab[user.genre]++;
		});
		return `
		    <section class="statsGenre"><h2>Utilisateurs par genre</h2><ul><li>homme: ${
					genreTab[0]
				} (${((genreTab[0] / nbUser) * 100).toFixed(2)}%)</li><li>femme: ${
			genreTab[1]
		} (${((genreTab[1] / nbUser) * 100).toFixed(2)}%)</li><li>autre: ${
			genreTab[2]
		} (${((genreTab[2] / nbUser) * 100).toFixed(2)}%)</li></ul></section>`;
	}
	getStatAge() {
		const nbUser = this.users.length;
		const ageTab = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		let somme = 0;
		this.users.forEach(user => {
			somme += user.age;
			if (user.age > 100) {
				ageTab[9]++;
			} else {
				ageTab[Math.floor((user.age - 5) / 10) - 1]++;
			}
		});
		return `<section class="statsAge"><h2>Utilisateurs par tranche d'âge</h2>
		Moyenne d'age: ${(somme / nbUser).toFixed(2)} ans</br>
		<ul><li>15-24 ans : ${ageTab[0]} (${((ageTab[0] / nbUser) * 100).toFixed(
			2
		)}%)</li>
		<li>25-34 ans : ${ageTab[1]} (${((ageTab[1] / nbUser) * 100).toFixed(2)}%)</li>
		<li>35-44 ans : ${ageTab[2]} (${((ageTab[2] / nbUser) * 100).toFixed(2)}%)</li>
		<li>45-54 ans : ${ageTab[3]} (${((ageTab[3] / nbUser) * 100).toFixed(2)}%)</li>
		<li>55-64 ans : ${ageTab[4]} (${((ageTab[4] / nbUser) * 100).toFixed(2)}%)</li>
		<li>64-74 ans : ${ageTab[5]} (${((ageTab[5] / nbUser) * 100).toFixed(2)}%)</li>
		<li>75-84 ans : ${ageTab[6]} (${((ageTab[6] / nbUser) * 100).toFixed(2)}%)</li>
		<li>85-94 ans : ${ageTab[7]} (${((ageTab[7] / nbUser) * 100).toFixed(2)}%)</li>
		<li>95+   ans : ${ageTab[8]} (${((ageTab[8] / nbUser) * 100).toFixed(2)}%)</li>
		</ul></section>`;
	}

	setLink() {
		const links = this.element.querySelectorAll('a:not(.autreAdmin)');
		links.forEach(link => {
			link.addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(event.currentTarget.getAttribute('href'));
			});
		});
	}

	getStatDateMois() {
		let res = '<section class="statsDateMois"><h2>Rendez-vous par mois</h2>';
		const rdvByMonthOfThisYear = this.getDataOfAYear();
		const rdvByMonthOfTheLastYear = this.getDataOfAYear(
			new Date().getFullYear() - 1
		);
		const averageRdvByMonth = this.getAverageRdvByMonth();
		const mois = [
			'Janvier  ',
			'Fevrier  ',
			'Mars     ',
			'Avril    ',
			'Mai      ',
			'Juin     ',
			'Juillet  ',
			'Aout     ',
			'Septembre',
			'Octobre  ',
			'Novembre ',
			'Decembre ',
		];
		res += `<table class="table table-striped">
		<thead class="thead-dark"><tr><th>Mois</th><th>${
			new Date().getFullYear() - 1
		}</th><th>${new Date().getFullYear()}</th><th>Moyenne</th></tr></thead>`;
		for (let i = 0; i < 12; i++) {
			res += `<tr>
					<th>${mois[i]}</th>
					<th>${rdvByMonthOfTheLastYear[i]}</th>
					<th>${rdvByMonthOfThisYear[i]}</th>
					<th>${averageRdvByMonth[i]}</th>
				<tr>`;
		}
		res += '</table></section>';

		return res;
	}

	getDateData() {
		const res = [];
		this.rdvs.forEach(rdv => {
			//const datePart = this.rdvs[0].date.split('-');
			//res.push(new Date(datePart[2], datePart[1], datePart[0]));
			res.push(new Date(rdv.date));
		});
		return res;
	}

	getDataOfAYear(yearWanted) {
		const year = yearWanted == null ? new Date().getFullYear() : yearWanted;
		const res = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		this.dateData.forEach(date => {
			if (parseInt(date.getFullYear()) == year) {
				res[date.getMonth()]++;
			}
		});

		return res;
	}

	getAverageRdvByMonth() {
		const somme = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		const anneesByMonth = [[], [], [], [], [], [], [], [], [], [], [], []];
		this.dateData.forEach(date => {
			if (!anneesByMonth[date.getMonth()].includes(date.getFullYear())) {
				anneesByMonth[date.getMonth()].push(date.getFullYear());
			}
			somme[date.getMonth()] += 1;
		});
		for (let i = 0; i < somme.length; i++) {
			somme[i] =
				anneesByMonth[i].length == 0 ? 0 : somme[i] / anneesByMonth[i].length;
		}

		return somme;
	}
	getStatsYear() {
		return `<section class="statsYear">
		<h2>Nombre de rendez-vous par année</h2>
		<section class="buttonSection">
		<button id="prevButton"><</button>
		<button id="actualYearButton">Année actuel</button>
		<button id="nextButton">></button></section>
		${this.getStatsDateYearBetween(
			new Date().getFullYear() - 10,
			new Date().getFullYear()
		)}>
		</section>`;
	}
	getStatsDateYearBetween(debut, fin) {
		const annees = [];
		for (let i = debut; i <= fin; i++) {
			annees.push(0);
		}
		for (let i = 0; i < this.dateData.length; i++) {
			if (
				this.dateData[i].getFullYear() >= debut &&
				this.dateData[i].getFullYear() <= fin
			) {
				annees[this.dateData[i].getFullYear() - debut]++;
			}
		}
		let res = `
		<table id="yearTable"><tr><th>Annee</th>`;
		for (let i = 0; i < annees.length; i++) {
			res += `<th>${i + debut}</th>`;
		}
		res += '</tr><tr><th>Nombre de rendez-vous</th>';
		for (let i = 0; i < annees.length; i++) {
			res += `<th>${annees[i]}</th>`;
		}
		res += '</tr></table';
		return res;
	}
}
