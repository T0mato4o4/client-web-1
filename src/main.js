import { getApiUrl } from './utils/url';
import Router from './Router';
import Connexion from './pages/Connexion';
import Menu from './pages/Menu';
import RdvTablePage from './pages/RdvTablePage';
import UsersPage from './pages/UsersPage';
import StatCategoriesPage from './pages/StatCategoriesPage';
import StatsPage from './pages/StatsPage';
import Page from './pages/Page';

const pageContainer = document.querySelector('.pageContainer'),
	apiUrl = getApiUrl(window.location);

/*fetch(`${apiUrl}/myresource`)
	.then(response => response.text())
	.then(message => (appContainer.innerHTML = message));
*/
const connexion = new Connexion();
const menu = new Menu();
const rdvTable = new RdvTablePage();
const usersTable = new UsersPage();
const statTable = new StatCategoriesPage();
const statsPage = new StatsPage(); /*
const userTable = new TablePage('user');
const categorietable = new TablePage('categorie');
const sousCategorieTable = new TablePage('sousCategorie');
const lieuTable = new TablePage('lieu');*/

Router.routes = [
	{ path: '/', page: rdvTable, title: 'rdvTable' },
	{ path: '/menu', page: menu, title: 'Menu' },
	{ path: '/connexion', page: connexion, title: 'Se connecter' },
	{ path: '/rdvTable', page: rdvTable, title: 'rdvTable' },
	{ path: '/users', page: usersTable, title: 'usersTable' },
	{ path: '/stat/a', page: statsPage, title: 'StatCategoriesTable' },
	{ path: '/stats', page: statsPage, title: 'Stats' },
];
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = pageContainer;
Router.menuElement = document.querySelector('.mainMenu');
Router.navigate(document.location.pathname);
window.onpopstate = () => {
	Router.navigate(document.location.pathname, false);
};
