import users from '../../ressources/users.json';
export default class User {
	static usersRessource;
	static usersList;
	id;
	nom;
	prenom;
	age;
	genre;
	constructor(id, nom, prenom, age, genre) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.genre = genre;
	}
	getGenre() {
		let res;
		if (this.genre == 0) {
			res = 'homme';
		} else if (this.genre == 1) {
			res = 'femme';
		} else if (this.genre == 2) {
			res = 'autre';
		} else {
			res = 'autre';
		}
		return res;
	}

	static getGenre(genre) {
		let res;
		if (genre == 0) {
			res = 'homme';
		} else if (genre == 1) {
			res = 'femme';
		} else if (genre == 2) {
			res = 'autre';
		} else {
			res = 'autre';
		}
		return res;
	}

	static getUser(id) {
		this.getUserList();
		let res = null;
		let idx = 0;
		while (idx < this.usersList.length && res == null) {
			if (id == users[idx].id) {
				res = this.usersList[idx];
			}
			idx++;
		}
		return res;
	}

	static getUserList() {
		if (this.usersList == null || this.usersRessource != users) {
			const res = [];
			this.usersRessource = users;
			this.usersRessource.forEach(userJSON => {
				const user = new User(
					userJSON.id_veictorien,
					userJSON.nom,
					userJSON.prenom,
					userJSON.age,
					userJSON.genre
				);
				res.push(user);
			});
			this.usersList = res;
		}
		return this.usersList;
	}
}
