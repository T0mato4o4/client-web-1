import vente from '../../ressources/vente.json';
import User from './User';
export default class RendezVous {
	static rdvRessource;
	static rdvList;
	id;
	vendeur;
	acheteur;
	date;
	lieu;

	constructor(id, vendeur, acheteur, date, lieu) {
		this.id = id;
		this.vendeur = vendeur;
		this.acheteur = acheteur;
		this.date = date;
		this.lieu = lieu;
	}

	getSimpleDate() {
		return `${this.date.getDate()}-${this.date.getMonth()}-${this.date.getFullYear()}`;
	}
	getLinkDate() {
		const day =
			this.date.getDate() < 10
				? `0${this.date.getDate()}`
				: this.date.getDate();

		const month =
			this.date.getMonth() < 10
				? `0${this.date.getMonth()}`
				: this.date.getMonth();
		return `${day}-${month}-${this.date.getFullYear()}`;
	}
	getCompleteDate() {
		const jours = [
			'Lundi',
			'Mardi',
			'Mercredi',
			'Jeudi',
			'Vendredi',
			'Samedi',
			'Dimanche',
		];

		const mois = [
			'Janvier  ',
			'Fevrier  ',
			'Mars     ',
			'Avril    ',
			'Mai      ',
			'Juin     ',
			'Juillet  ',
			'Aout     ',
			'Septembre',
			'Octobre  ',
			'Novembre ',
			'Decembre ',
		];
		return `${jours[this.date.getDay()]} ${this.date.getDate()} ${
			mois[this.date.getMonth()]
		} ${this.date.getFullYear()} ${this.date.getHours()}:${this.date.getMinutes()}`;
	}

	/*	static createFromJSON(object) {
		return new RendezVous(
			object.id,
			User.getUser(object.idVendeur),
			User.getUser(object.idAcheteur),
			object.categorie,
			object.sousCat,
			object.nomProduit,
			new Date(object.date),
			object.lieu
		);
	}
*/
	static getRdvList() {
		if (this.rdvList == null || this.rdvRessource != vente) {
			const res = [];
			this.rdvRessource = vente;
			this.rdvRessource.forEach(rdvJSON => {
				res.push(this.createFromJSON(rdvJSON));
			});
			this.rdvList = res;
		}
		return this.rdvList;
	}
	static createRdvList(rdvJSONList) {
		const list = [];
		rdvJSONList.forEach(rdvJSON => {
			console.log(rdvJSON.date);
			list.push(
				new RendezVous(
					rdvJSON.id,
					rdvJSON.vendeur,
					rdvJSON.acheteur,
					new Date(rdvJSON.date),
					rdvJSON.lieux
				)
			);
		});
		return list;
	}
	static createFromRdvAndDetails(rdvJSON, detailsJSON) {
		//const datePart = rdvJSON.date.split('-');
		console.log(rdvJSON);
		return new RendezVous(
			rdvJSON.id,
			detailsJSON.vendeur,
			detailsJSON.acheteur,
			new Date(/*datePart[2], datePart[1], datePart[0]*/ rdvJSON.date),
			rdvJSON.lieux
		);
	}
}
