export default class Categorie {
	name;
	sousCat = [];

	constructor(name, sousCat) {
		this.name = name;
		this.sousCat = sousCat;
	}
}
