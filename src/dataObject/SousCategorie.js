import categories from '../../ressources/cat.json';

export default class sousCategorie {
	name;

	constructor(name) {
		this.name = name;
	}

	static getAllSousCateg() {
		let tab = [];
		categories.forEach(cat => {
			cat.sousCat.forEach(sousCate => {
				tab.push(sousCate);
			});
		});
		return tab;
	}
}
