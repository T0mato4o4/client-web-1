import { lineBreak } from 'acorn';
import PageNotFoud from './pages/PageNotFound';
import User from './dataObject/User';
export default class Router {
	static pageNotFoud = new PageNotFoud();
	static titleElement;
	static contentElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	// C.3. Navigation en JS : Le menu
	static #menuElement; // propriété statique privée
	/**
	 * Setter qui indique au Router la balise HTML contenant le menu de navigation.
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate.
	 * @param element Élément HTML qui contient le menu principal
	 */
	static set menuElement(element) {
		this.#menuElement = element;
		const links = this.#menuElement.querySelectorAll('a');
		links.forEach(link =>
			link.addEventListener('click', event => {
				event.preventDefault();
				this.navigate(event.currentTarget.getAttribute('href'));
			})
		);
	}

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 * @param {Boolean} pushState active/désactive le pushState (ajout d'une entrée dans l'historique de navigation)
	 */
	static navigate(path, pushState = true) {
		const pathPart = path.split('/');
		const route = this.routes.find(route => route.path === '/' + pathPart[1]);

		const admins = [];
		if (route) {
			fetch('http://veictor11.azae.eu/api/v1/utilisateurs/admins')
				.then(response => response.json())
				.then(listAdmin => {
					const cookieArray = document.cookie.split('; ');
					let trouve = false;
					let mdp = '';
					let pseudo = '';
					let idx = 0;

					while (idx < cookieArray.length && (mdp === '' || pseudo === '')) {
						const cookie = cookieArray[idx].split('=');
						if (cookie[0] === 'VeictorMdp') {
							mdp = cookie[1];
						} else if (cookie[0] === 'VeictorPseudo') {
							pseudo = cookie[1];
						}
						idx++;
					}
					if (
						this.checkLogin(listAdmin, pseudo, mdp) ||
						route.path === '/connexion'
					) {
						// this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
						//this.contentElement.innerHTML = route.page.render();
						// D.2. Préparatifs : La classe Page
						if (pathPart.length > 2) {
							route.page.mount?.(this.contentElement, pathPart);
						} else {
							route.page.mount?.(this.contentElement);
						}

						// E.1. Activation du menu
						const previousMenuLink = this.#menuElement.querySelector('.active'),
							newMenuLink = this.#menuElement.querySelector(
								`a[href="${path}"]`
							);
						previousMenuLink?.classList.remove('active'); // on retire la classe "active" du précédent menu
						newMenuLink?.classList.add('active'); // on ajoute la classe CSS "active" sur le nouveau lien

						// E.2. History API
						if (pushState) {
							window.history.pushState(null, null, path);
						}
					} else {
						Router.navigate('/connexion');
					}
				});
		} else {
			this.pageNotFoud.mount(this.contentElement);
		}
	}

	static checkLogin(admins, pseudo, mdp) {
		let res = false;
		admins.forEach(anAdmin => {
			if (
				anAdmin.admin == true &&
				pseudo == anAdmin.nom &&
				mdp == anAdmin.mdp
			) {
				res = anAdmin.nom === pseudo && anAdmin.mdp === mdp;
			}
		});
		return res;
	}
}
