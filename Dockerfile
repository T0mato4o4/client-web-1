FROM node:12
ADD package.json /app/
ADD package-lock.json /app/
WORKDIR /app/
RUN npm install
ADD . /app/
RUN npm run build
EXPOSE 8000
CMD npm start
